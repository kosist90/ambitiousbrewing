﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Application Dependencies" Type="Folder">
			<Item Name="System Types.lvlib" Type="Library" URL="../Application Dependencies/System Types/System Types.lvlib"/>
			<Item Name="System Paths.lvlib" Type="Library" URL="../Application Dependencies/System Paths/System Paths.lvlib"/>
			<Item Name="Buttons.lvlib" Type="Library" URL="../Application Dependencies/Application Controls/Buttons.lvlib"/>
			<Item Name="Operator Dialog.lvlib" Type="Library" URL="../Application Dependencies/Operator Dialog/Operator Dialog.lvlib"/>
		</Item>
		<Item Name="Models" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Hardware Type Models" Type="Folder">
				<Item Name="Interfaces" Type="Folder">
					<Item Name="IHeaterModel.lvlib" Type="Library" URL="../Models/Interfaces/IHeaterModel/IHeaterModel.lvlib"/>
					<Item Name="IPumpModel.lvlib" Type="Library" URL="../Models/Interfaces/IPumpModel/IPumpModel.lvlib"/>
					<Item Name="ITemperatureModel.lvlib" Type="Library" URL="../Models/Interfaces/ITemperatureModel/ITemperatureModel.lvlib"/>
					<Item Name="IValveModel.lvlib" Type="Library" URL="../Models/Interfaces/IValveModel/IValveModel.lvlib"/>
					<Item Name="ILimitSwitchModel.lvlib" Type="Library" URL="../Models/Interfaces/ILimitSwitchModel/ILimitSwitchModel.lvlib"/>
				</Item>
				<Item Name="HeaterModel.lvlib" Type="Library" URL="../Models/HeaterModel/HeaterModel.lvlib"/>
				<Item Name="PumpModel.lvlib" Type="Library" URL="../Models/PumpModel/PumpModel.lvlib"/>
				<Item Name="TemperatureModel.lvlib" Type="Library" URL="../Models/TemperatureModel/TemperatureModel.lvlib"/>
				<Item Name="ValveModel.lvlib" Type="Library" URL="../Models/ValveModel/ValveModel.lvlib"/>
				<Item Name="LimitSwitchModel.lvlib" Type="Library" URL="../Models/LimitSwitchModel/LimitSwitchModel.lvlib"/>
			</Item>
			<Item Name="System Component Models" Type="Folder">
				<Item Name="Interfaces" Type="Folder">
					<Item Name="IHotLiquorTank.lvlib" Type="Library" URL="../Models/Interfaces/IHotLiquorTank/IHotLiquorTank.lvlib"/>
					<Item Name="IMashTun.lvlib" Type="Library" URL="../Models/Interfaces/IMashTun/IMashTun.lvlib"/>
					<Item Name="IBoilKettle.lvlib" Type="Library" URL="../Models/Interfaces/IBoilKettle/IBoilKettle.lvlib"/>
					<Item Name="IPlumbing.lvlib" Type="Library" URL="../Models/Interfaces/IPlumbing/IPlumbing.lvlib"/>
					<Item Name="IChiller.lvlib" Type="Library" URL="../Models/Interfaces/IChiller/IChiller.lvlib"/>
				</Item>
				<Item Name="HotLiquorTank.lvlib" Type="Library" URL="../Models/HotLiquorTank/HotLiquorTank.lvlib"/>
				<Item Name="MashTun.lvlib" Type="Library" URL="../Models/MashTun/MashTun.lvlib"/>
				<Item Name="BoilKettle.lvlib" Type="Library" URL="../Models/BoilKettle/BoilKettle.lvlib"/>
				<Item Name="ManualPlumbing.lvlib" Type="Library" URL="../Models/Plumbing/ManualPlumbing.lvlib"/>
				<Item Name="SimulatedChiller.lvlib" Type="Library" URL="../Models/Chiller/SimulatedChiller.lvlib"/>
			</Item>
			<Item Name="Sequencing" Type="Folder">
				<Item Name="Test Steps" Type="Folder">
					<Property Name="NI.SortType" Type="Int">3</Property>
					<Item Name="ITestStep" Type="Folder">
						<Item Name="ITestStep.lvlib" Type="Library" URL="../Models/Test Steps/ITestStep/ITestStep.lvlib"/>
					</Item>
					<Item Name="unused" Type="Folder">
						<Item Name="Prompt Operator Step.lvlib" Type="Library" URL="../Models/Test Steps/Prompt Operator Step/Prompt Operator Step.lvlib"/>
						<Item Name="Actuate Valve.lvlib" Type="Library" URL="../Models/Test Steps/Actuate Valve/Actuate Valve.lvlib"/>
						<Item Name="Fill Step.lvlib" Type="Library" URL="../Models/Test Steps/Fill Step/Fill Step.lvlib"/>
						<Item Name="Drain Step.lvlib" Type="Library" URL="../Models/Test Steps/Drain Step/Drain Step.lvlib"/>
					</Item>
					<Item Name="Wait Step.lvlib" Type="Library" URL="../Models/Test Steps/Wait Step/Wait Step.lvlib"/>
					<Item Name="Preheat Step.lvlib" Type="Library" URL="../Models/Test Steps/Preheat Step/Preheat Step.lvlib"/>
					<Item Name="Mash Step.lvlib" Type="Library" URL="../Models/Test Steps/Mash Step/Mash Step.lvlib"/>
					<Item Name="Lauter Step.lvlib" Type="Library" URL="../Models/Test Steps/Lauter Step/Lauter Step.lvlib"/>
					<Item Name="Boil Step.lvlib" Type="Library" URL="../Models/Test Steps/Boil Step/Boil Step.lvlib"/>
					<Item Name="Cool Step.lvlib" Type="Library" URL="../Models/Test Steps/Cool Step/Cool Step.lvlib"/>
				</Item>
				<Item Name="AmbitiousBrewing ProcessModel.lvlib" Type="Library" URL="../Models/ProcessModel/AmbitiousBrewing ProcessModel.lvlib"/>
			</Item>
			<Item Name="ABC Model.lvlib" Type="Library" URL="../Models/ABC Model/ABC Model.lvlib"/>
		</Item>
		<Item Name="Views" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="RecipeView" Type="Folder">
				<Item Name="NestedViews" Type="Folder">
					<Item Name="RecipeButtons.lvlib" Type="Library" URL="../Views/RecipeView/RecipeButtons/RecipeButtons.lvlib"/>
					<Item Name="RecipeListBoxView.lvlib" Type="Library" URL="../Views/RecipeView/RecipeListbox/RecipeListBoxView.lvlib"/>
				</Item>
				<Item Name="RecipeView.lvlib" Type="Library" URL="../Views/RecipeView/RecipeView.lvlib"/>
			</Item>
			<Item Name="SettingsView" Type="Folder">
				<Item Name="SettingsEditor" Type="Folder"/>
				<Item Name="SettingsListbox" Type="Folder"/>
				<Item Name="SettingsView.lvlib" Type="Library" URL="../Views/SettingsView/SettingsView.lvlib"/>
			</Item>
			<Item Name="ControlView.lvlib" Type="Library" URL="../Views/SystemControlView/ControlView/ControlView.lvlib"/>
			<Item Name="MainView.lvlib" Type="Library" URL="../Views/MainView/MainView.lvlib"/>
			<Item Name="NavigationView.lvlib" Type="Library" URL="../Views/NavigationView/NavigationView.lvlib"/>
			<Item Name="SimulatedIOView.lvlib" Type="Library" URL="../Views/SimulatedIOView/SimulatedIOView.lvlib"/>
			<Item Name="SystemView.lvlib" Type="Library" URL="../Views/SystemControlView/SystemView/SystemView.lvlib"/>
			<Item Name="TemperaturePlotView.lvlib" Type="Library" URL="../Views/TemperaturePlotView/TemperaturePlotView.lvlib"/>
		</Item>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="configuration" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="IniFileSection" Type="Folder">
							<Item Name="IniFileSection.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/IniFileSection/IniFileSection.lvclass"/>
						</Item>
						<Item Name="TransientConfiguration" Type="Folder">
							<Item Name="TransientConfiguration.lvclass" Type="LVClass" URL="../gpm_packages/@cs/configuration/Source/TransientConfiguration/TransientConfiguration.lvclass"/>
						</Item>
						<Item Name="Configuration.lvlib" Type="Library" URL="../gpm_packages/@cs/configuration/Source/Configuration.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/configuration/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/configuration/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/configuration/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/configuration/README.md"/>
				</Item>
				<Item Name="event-logger" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Composed Log" Type="Folder">
							<Item Name="Composed Log.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Composed Log/Composed Log.lvlib"/>
						</Item>
						<Item Name="IStringFormat" Type="Folder">
							<Item Name="IStringFormat.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/IStringFormat/IStringFormat.lvclass"/>
						</Item>
						<Item Name="LVQueue Sink" Type="Folder">
							<Item Name="LVQueue Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/LVQueue Sink/LVQueue Sink.lvclass"/>
						</Item>
						<Item Name="Network Sink" Type="Folder">
							<Item Name="Network Sink.lvlib" Type="Library" URL="../gpm_packages/@cs/event-logger/Source/Network Sink/Network Sink.lvlib"/>
						</Item>
						<Item Name="String Control Sink" Type="Folder">
							<Item Name="String Control Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Control Sink/String Control Sink.lvclass"/>
						</Item>
						<Item Name="String Log Sink" Type="Folder">
							<Item Name="String Log Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/String Log Sink/String Log Sink.lvclass"/>
						</Item>
						<Item Name="SystemLog Sink" Type="Folder">
							<Item Name="SystemLog Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/SystemLog Sink/SystemLog Sink.lvclass"/>
						</Item>
						<Item Name="Text File Sink" Type="Folder">
							<Item Name="Text File Sink.lvclass" Type="LVClass" URL="../gpm_packages/@cs/event-logger/Source/Text File Sink/Text File Sink.lvclass"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/event-logger/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/event-logger/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/event-logger/LICENSE"/>
					<Item Name="Readme.md" Type="Document" URL="../gpm_packages/@cs/event-logger/Readme.md"/>
				</Item>
				<Item Name="listbox" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="ListboxExtensions.lvlib" Type="Library" URL="../gpm_packages/@cs/listbox/Source/ListboxExtensions.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/listbox/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/listbox/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/listbox/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/listbox/README.md"/>
				</Item>
				<Item Name="lookup-table" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="LookupTable" Type="Folder">
							<Item Name="LookupTable.lvlib" Type="Library" URL="../gpm_packages/@cs/lookup-table/Source/LookupTable/LookupTable.lvlib"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/lookup-table/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/lookup-table/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/README.md"/>
				</Item>
				<Item Name="variant" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="VariantExtensions.lvlib" Type="Library" URL="../gpm_packages/@cs/variant/Source/VariantExtensions.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/variant/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/variant/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/variant/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/variant/README.md"/>
				</Item>
				<Item Name="linked-list" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Linked List" Type="Folder">
							<Item Name="Linked Lists.lvlib" Type="Library" URL="../gpm_packages/@cs/linked-list/Source/Linked List/Linked Lists.lvlib"/>
						</Item>
						<Item Name="String Manipulation" Type="Folder">
							<Item Name="String Manipulation.lvlib" Type="Library" URL="../gpm_packages/@cs/linked-list/Source/String Manipulation/String Manipulation.lvlib"/>
						</Item>
					</Item>
					<Item Name="Error Codes.txt" Type="Document" URL="../gpm_packages/@cs/linked-list/Error Codes.txt"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/linked-list/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/linked-list/LICENSE"/>
					<Item Name="Linked Lists.lvproj" Type="Document" URL="../gpm_packages/@cs/linked-list/Linked Lists.lvproj"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/linked-list/README.md"/>
				</Item>
				<Item Name="mva-framework" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="DialogBox" Type="Folder">
							<Item Name="KeypadDialogBox" Type="Folder">
								<Item Name="KeypadDialogBox.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/DialogBox/KeypadDialogBox/KeypadDialogBox.lvlib"/>
							</Item>
							<Item Name="OneButtonDialogBox" Type="Folder">
								<Item Name="OneButtonDialogBox.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/DialogBox/OneButtonDialogBox/OneButtonDialogBox.lvlib"/>
							</Item>
							<Item Name="TwoButtonDialogBox" Type="Folder">
								<Item Name="TwoButtonDialogBox.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/DialogBox/TwoButtonDialogBox/TwoButtonDialogBox.lvlib"/>
							</Item>
						</Item>
						<Item Name="EventSinkConfigurations" Type="Folder">
							<Item Name="EventSinkConfigurations.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/EventSinkConfigurations/EventSinkConfigurations.lvlib"/>
						</Item>
						<Item Name="Framework" Type="Folder">
							<Item Name="AbstractMessages" Type="Folder">
								<Item Name="AbstractMessages.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/AbstractMessages/AbstractMessages.lvlib"/>
							</Item>
							<Item Name="ActorEvents" Type="Folder">
								<Item Name="ActorEvents.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/ActorEvents/ActorEvents.lvlib"/>
								<Item Name="AutoRegistration.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/ActorEvents/AutoRegistration.lvlib"/>
							</Item>
							<Item Name="IDialogBox" Type="Folder">
								<Item Name="IDialogBox.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IDialogBox/IDialogBox.lvlib"/>
							</Item>
							<Item Name="IMediator" Type="Folder">
								<Item Name="IMediator.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IMediator/IMediator.lvlib"/>
							</Item>
							<Item Name="IModel" Type="Folder">
								<Item Name="IModel.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IModel/IModel.lvlib"/>
							</Item>
							<Item Name="IObserver" Type="Folder">
								<Item Name="IObserver.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IObserver/IObserver.lvlib"/>
							</Item>
							<Item Name="IViewable" Type="Folder">
								<Item Name="IViewable.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IViewable/IViewable.lvlib"/>
							</Item>
							<Item Name="IViewManager" Type="Folder">
								<Item Name="IViewManager.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IViewManager/IViewManager.lvlib"/>
							</Item>
							<Item Name="IViewModel" Type="Folder">
								<Item Name="IViewModel.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/IViewModel/IViewModel.lvlib"/>
							</Item>
							<Item Name="Mediator" Type="Folder">
								<Item Name="Mediator.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/Mediator/Mediator.lvlib"/>
							</Item>
							<Item Name="PublicationPolicy" Type="Folder">
								<Item Name="PublicationPolicy.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/PublicationPolicy/PublicationPolicy.lvlib"/>
							</Item>
							<Item Name="SubscriptionPolicy" Type="Folder">
								<Item Name="SubscriptionPolicy.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/SubscriptionPolicy/SubscriptionPolicy.lvlib"/>
							</Item>
							<Item Name="Transport" Type="Folder">
								<Item Name="Transport.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/Framework/Transport/Transport.lvlib"/>
							</Item>
						</Item>
						<Item Name="ViewManager" Type="Folder">
							<Item Name="BoundViewManager" Type="Folder">
								<Item Name="BoundViewManager.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/ViewManager/BoundViewManager/BoundViewManager.lvlib"/>
							</Item>
							<Item Name="LeftListboxViewManager" Type="Folder">
								<Item Name="LeftListboxViewManager.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/ViewManager/LeftListboxViewManager/LeftListboxViewManager.lvlib"/>
							</Item>
							<Item Name="QuadViewManager" Type="Folder">
								<Item Name="QuadViewManager.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/ViewManager/QuadViewManager/QuadViewManager.lvlib"/>
							</Item>
							<Item Name="TabbedViewManager" Type="Folder">
								<Item Name="TabbedViewManager.lvlib" Type="Library" URL="../gpm_packages/@cs/mva-framework/Source/ViewManager/TabbedViewManager/TabbedViewManager.lvlib"/>
							</Item>
						</Item>
						<Item Name="MVA Error List.txt" Type="Document" URL="../gpm_packages/@cs/mva-framework/Source/MVA Error List.txt"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/mva-framework/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/mva-framework/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/mva-framework/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/mva-framework/README.md"/>
				</Item>
				<Item Name="test-executive" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Test Executive" Type="Folder">
							<Item Name="Test Executive.lvlib" Type="Library" URL="../gpm_packages/@cs/test-executive/Source/Test Executive/Test Executive.lvlib"/>
						</Item>
					</Item>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/test-executive/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/test-executive/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/test-executive/README.md"/>
					<Item Name="Test Executive.lvproj" Type="Document" URL="../gpm_packages/@cs/test-executive/Test Executive.lvproj"/>
				</Item>
			</Item>
		</Item>
		<Item Name="ABC ViewModel.lvlib" Type="Library" URL="../ViewModel/ABC ViewModel.lvlib"/>
		<Item Name="AssemblrLaunchr.vi" Type="VI" URL="../AssemblrLaunchr.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="LVRowAndColumnUnsignedTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnUnsignedTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
