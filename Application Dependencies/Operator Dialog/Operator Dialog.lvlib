﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16764057</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">15000804</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)W!!!*Q(C=\&gt;5R4A*"&amp;-&lt;R4W."SQU-.T$P#D4;=Q0T$G"$;UF"25T-OQ*8Y!LP#FS"SH\^T`!E%B*IV"DD,,0,@L-T]X.W7;6?2N+.BENF&gt;,&lt;U^NKR(`9HQV(4J]`Q=4B%*_VNH(0NJ`W0ZN`PBK/GM`Y,2@`FP`TN]HTR2`$NPY-LN2=2.;F"&gt;;KJ44ME?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:(XD6TE)B=ZJ'4S:+*EU'3!Z')I3H;**`%EHM4$K2*0YEE]C3@R=)E34_**0)EH]&gt;".C3@R**\%EXA9KEOS&lt;_2Y%A`$+`!%HM!4?!)05SLQ")"AMG$A9"!9#BK$,Y%H]!1?PCLQ"*\!%XA#$]U+0)%H]!3?Q%/8PCL2.7UDR]-Q=DS/R`%Y(M@$U()]DM@R/"\(QX2S0)\(14A4/I.$E.0*O=!Z=4S/BY-=D_.R0)\(]&gt;$5\Z$XF7G;NJ(D-4S'R`!9(M0$%$)]BM@Q'"\$Q\!S0)&lt;(]"A?Q].5-DS'R`!9%'.3JJ=RG.(2O-A)$!_@`L29PUP2*&gt;9XK2Z?V5/J?NB5$Z(KY6$&gt;&gt;.8.6.UEV?+L&amp;F7V7+J&amp;50VR+L1+IZJ%V&lt;F&gt;K"X\,86$86-8V$FV2JV3*^2R[`L&amp;&amp;_ZW/WWX7WUW'[X8;SU7#]XH=]VG-UWH5UUG%YX(Y].LY*&lt;N]%,9PZ??_&lt;Z]?LR&lt;0&lt;T=LR\?\NNR_@2[V`)@_0`]#^[.ON&lt;H/6CD&gt;VK\3;I!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Operator Dialog.vi" Type="VI" URL="../Operator Dialog.vi"/>
</Library>
