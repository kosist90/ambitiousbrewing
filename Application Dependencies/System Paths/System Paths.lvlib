﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">15000804</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)I!!!*Q(C=\&gt;8.=2J"%-8R:Z50PJ+"CB2?#K1!)81+8(5EB5["&amp;%CB5S!&amp;5J$`-^N#,J=E,L*+Z7*8$=P&lt;_@ARO[SEO@W3@OLZVL&lt;\="PHJ&gt;X9R`OMZ8A*/&lt;]EM]8,A:9WS`FOU.6^8];9]S]@L_^`^8_&gt;=P@G_2P_'ZPOWXX\P\?HGT_#@`Y\_+(R)+++3CII;UT\8/2&amp;8O2&amp;8O2&amp;HO2*HO2*HO2*(O2"(O2"(O2"&lt;H+4G^TE*D@ZX-F&amp;,H+21SIG,S9K"CU'+"J$5@&amp;3?!J0Y3E]@&amp;4B+4S&amp;J`!5(JKI]"3?QF.Y#A`&gt;6(A+4_%J0)7(I;;EZE[/J`!QP"*0YEE]C3@R-+534Q*)*EM'4A;"I?2E=J"Y%E`CY6#**`%EHM34?$CNR*.Y%E`C34RUG;O35T.W=DQ-I]!4?!*0Y!E]$+X!%XA#4_!*0%SHQ".Y!E1Q94!Y"!7&gt;AA&lt;"B]!4?(B4Y!E]A3@Q""Z/T3M5=W7':OTE?)T(?)T(?)S()71]RG-]RG-]$#PD-2\D-2\D93I:D`%9DY'93:F?:D$4U41SA@(Q._]7T[O55_+Z3XXTKG^+^=WGPIH5.Y@[IKMPJPICK2&gt;@P;DKR6)PAPL,K&gt;&amp;KD(I3&gt;?@25"&gt;?T^3*/F)(;E^NK1WVJF;D[S=XP&amp;QO/J`0/JV//B[0/BQ/WO`XWG[XWGQW7K`87KV7V]@!)`PVA&lt;!]FZYY@K__Y0`T.XAW[E&amp;`TM%;`1&lt;U`TI:!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Log Directory.vi" Type="VI" URL="../Log Directory.vi"/>
	<Item Name="Recipe Directory.vi" Type="VI" URL="../Recipe Directory.vi"/>
	<Item Name="Settings Directory.vi" Type="VI" URL="../Settings Directory.vi"/>
</Library>
