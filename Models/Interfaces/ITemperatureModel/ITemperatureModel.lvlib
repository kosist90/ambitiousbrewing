﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16757617</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">15000804</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16762993</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)[!!!*Q(C=\&gt;8.&lt;3*"%)&lt;B$]M(=S3$&amp;3F5"*9Y\'(F'SF5#FRN4MC/I&amp;*A1S#&amp;CG!F5JA5:N^OSMA`-FRMS\,=1Q0T^&gt;`DHG%M^8)F87I]6[9H3\70U^;TV_0:]`:[48OXRZ/8\@VD0$X_-0_L^1`DRT@;4`D0&amp;0W5H`+^S_X:(]'(`QYG;A]C;F+$[F247X:-]C20]C20]C10]C!0]C!0]C"X=C&gt;X=C&gt;X=C=X=C-X=C-X=C0P"\H)23ZS3-HCS5,*J-E%37=I3NY34_**0)G(5S7?R*.Y%E`CI9M34_**0)EH]4"-C3@R**\%EXC9KEOS(_2Y%A`4+`!%HM!4?!)03SLQ")"AM7$C9")9#BK$,Y%H]!1?PCLQ"*\!%XA#$]U+0)%H]!3?Q-/1PCP2.?UAR]-U=DS/R`%Y(M@$V()]DM@R/"\(QX*S0)\(14A,/J.$E$0)[?#=/"\(QY==D_.R0)\(]&gt;$5LZ$XH7G;&gt;J$D-4S'R`!9(M0$&amp;$)]BM@Q'"\$Q\1S0)&lt;(]"A?Q].3-DS'R`!9%'.2FJ=RG4(1['1%BI&gt;8PVOM8[8I%OO(6$?P[K:5X7SKGUBV=[AOOOJCKC[3;P.6G[L;,.5GK0YY&amp;6K&amp;53WC'NQ[;O"^4^V2N^1.&gt;56&gt;5B@5/887BLZTRW%9N.`PN&gt;PNN.VON&gt;FMN&amp;KNN&amp;QON6AM.*`0.:P.DI_"8RT("],BO84,^`8Q=,@_`?^[`?=PH`?4^=X^J/7@]0`Z#TQ&lt;&gt;;'H;\"(`Q%^&amp;$`:!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Messages" Type="Folder">
		<Item Name="Read Temperature Msg.lvclass" Type="LVClass" URL="../../ITemperatureModel Messages/Read Temperature Msg/Read Temperature Msg.lvclass"/>
	</Item>
	<Item Name="ITemperatureModel.lvclass" Type="LVClass" URL="../ITemperatureModel.lvclass"/>
</Library>
