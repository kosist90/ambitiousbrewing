﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6579300</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">15000804</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&amp;!!!*Q(C=\&gt;7\&lt;2N2%)8B9U/"UY5D:Q*&lt;G"99+44!R-KH";:7RB9'[I!NX!I%4!-+NA)$4"WO`XMZJAA:%"0,-!4N=J&lt;=M`@R;6_3RP**ON*S;&lt;F\=?'Y&gt;0R5M9[0RG::_,I\KW0DM&gt;=XP8^P&gt;X[]7IS&gt;0L_?^4^N+-&lt;`M``P_88M`\,`QK,XZ8VZW]PXCQ`"KT]((`K&lt;9K'3#MIJ'U`XEO2*HO2*HO2*(O2"(O2"(O2"\O2/\O2/\O2/&lt;O2'&lt;O2'&lt;O2'0F:SE9N=Z*#3S:/*EE'4!:,'5*2M%E`C34S*BVUFHM34?"*0YK'*%E`C34S**`(149EH]33?R*.Y''J)=KTE?")0QSPQ"*\!%XA#$V-K]!3!9,*AY'!1'!I/"D]#4_!*00R5Y!E]A3@Q""Y/+`!%HM!4?!)08=::C;(J+TE?BJ(D=4S/R`%Y(I;7YX%]DM@R/"[GE_.R0!\#G&gt;!:()+=4EY$:]@R/"[_Z(A=D_.R0)[(1_-+_4AT8&gt;.8=DS'R`!9(M.D?"B#BM@Q'"\$9XA96I&lt;(]"A?QW.YG%K'R`!9(A.C4-LU-A9T/BK.D-$Q]"FXCYWL&amp;%.C9Z8KZF8&gt;F+K&lt;4854K7Y/V56885T626+&gt;@.6*6:UMV5F1`8%KN!KDGE46O4@5A?V-.7J0\;ANN;(7V)K;?N?`X0"Q/'C?:\87N.`PN&gt;PNN.VON&gt;FMN&amp;[PN6KN.%X4[46QT8J[)4S^FR[GWS_NX@R]&lt;&amp;`P@\2P=\4&lt;T``I``.`]'\52ZX0Q4H["&gt;@:8VI!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="SettingsView.lvclass" Type="LVClass" URL="../SettingsView.lvclass"/>
</Library>
